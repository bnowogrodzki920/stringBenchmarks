﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BenchmarkDotNet;
using BenchmarkDotNet.Running;

namespace String
{
    class Program
    {
        static void Main(string[] args)
        {
            BenchmarkRunner.Run<StringBenchmarks>();
            BenchmarkRunner.Run<StringWithReturnBenchmarks>();
        }
    }
}
