﻿using System.Text;
using BenchmarkDotNet.Attributes;

namespace String
{
    [MemoryDiagnoser, 
     RankColumn]
    public class StringBenchmarks 
    {
        [Benchmark]
        public void StringAdd10Times()
        {
            string build = "";

            for (int i = 0; i < 10; i++)
            {
                build += "dupa";
            }
        }

        [Benchmark]
        public void StringAdd100Times()
        {
            string build = "";

            for (int i = 0; i < 100; i++)
            {
                build += "dupa";
            }
        }

        [Benchmark]
        public void StringAdd1000Times()
        {
            string build = "";

            for (int i = 0; i < 1000; i++)
            {
                build += "dupa";
            }
        }

        [Benchmark]
        public void StringBuilderAppend10Times()
        {
            StringBuilder stringBuilder = new StringBuilder();

            for (int i = 0; i < 10; i++)
            {
                stringBuilder.Append("dupa");
            }
        }

        [Benchmark]
        public void StringBuilderAppend100Times()
        {
            StringBuilder stringBuilder = new StringBuilder();

            for (int i = 0; i < 100; i++)
            {
                stringBuilder.Append("dupa");
            }
        }

        [Benchmark]
        public void StringBuilderAppend1000Times()
        {
            StringBuilder stringBuilder = new StringBuilder();

            for (int i = 0; i < 1000; i++)
            {
                stringBuilder.Append("dupa");
            }
        }
    }    
}