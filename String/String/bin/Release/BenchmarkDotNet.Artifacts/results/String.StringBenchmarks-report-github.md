``` ini

BenchmarkDotNet=v0.13.1, OS=Windows 10.0.19043.1288 (21H1/May2021Update)
Intel Core i7-9750H CPU 2.60GHz, 1 CPU, 12 logical and 6 physical cores
  [Host]     : .NET Framework 4.8 (4.8.4420.0), X86 LegacyJIT
  DefaultJob : .NET Framework 4.8 (4.8.4420.0), X86 LegacyJIT


```
|                       Method |         Mean |       Error |      StdDev | Rank |    Gen 0 |   Gen 1 |   Allocated |
|----------------------------- |-------------:|------------:|------------:|-----:|---------:|--------:|------------:|
|             StringAdd10Times |     267.7 ns |     5.19 ns |     5.10 ns |    2 |   0.1097 |       - |       577 B |
|            StringAdd100Times |   9,363.5 ns |   145.22 ns |   128.73 ns |    4 |   8.0109 |       - |    42,040 B |
|           StringAdd1000Times | 254,796.1 ns | 2,403.43 ns | 2,248.17 ns |    5 | 767.0898 | 13.6719 | 4,027,791 B |
|   StringBuilderAppend10Times |     150.8 ns |     0.95 ns |     0.84 ns |    1 |   0.0472 |       - |       248 B |
|  StringBuilderAppend100Times |   1,076.6 ns |     6.75 ns |     6.31 ns |    3 |   0.2403 |       - |     1,266 B |
| StringBuilderAppend1000Times |   9,379.4 ns |    56.34 ns |    52.70 ns |    4 |   1.6174 |  0.0305 |     8,565 B |
