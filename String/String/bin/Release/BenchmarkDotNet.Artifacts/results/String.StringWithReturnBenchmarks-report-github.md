``` ini

BenchmarkDotNet=v0.13.1, OS=Windows 10.0.19043.1288 (21H1/May2021Update)
Intel Core i7-9750H CPU 2.60GHz, 1 CPU, 12 logical and 6 physical cores
  [Host]     : .NET Framework 4.8 (4.8.4420.0), X86 LegacyJIT
  DefaultJob : .NET Framework 4.8 (4.8.4420.0), X86 LegacyJIT


```
|                       Method |         Mean |       Error |      StdDev | Rank |    Gen 0 |   Gen 1 |   Allocated |
|----------------------------- |-------------:|------------:|------------:|-----:|---------:|--------:|------------:|
|             StringAdd10Times |     260.8 ns |     1.09 ns |     0.97 ns |    2 |   0.1097 |       - |       577 B |
|            StringAdd100Times |   9,180.9 ns |    34.77 ns |    30.83 ns |    4 |   8.0109 |  0.0305 |    42,040 B |
|           StringAdd1000Times | 251,738.9 ns | 2,225.93 ns | 2,082.14 ns |    6 | 767.0898 | 33.2031 | 4,027,791 B |
|   StringBuilderAppend10Times |     199.1 ns |     0.95 ns |     0.84 ns |    1 |   0.0656 |       - |       345 B |
|  StringBuilderAppend100Times |   1,274.6 ns |     4.31 ns |     3.82 ns |    3 |   0.3967 |  0.0019 |     2,083 B |
| StringBuilderAppend1000Times |  10,412.6 ns |    64.62 ns |    60.44 ns |    5 |   3.1586 |  0.1526 |    16,598 B |
