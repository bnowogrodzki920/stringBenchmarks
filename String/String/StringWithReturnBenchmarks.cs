﻿using System.Text;
using BenchmarkDotNet.Attributes;

namespace String
{
    [MemoryDiagnoser, 
     RankColumn]
    public class StringWithReturnBenchmarks 
    {
        [Benchmark]
        public string StringAdd10Times()
        {
            string build = "";

            for (int i = 0; i < 10; i++)
            {
                build += "dupa";
            }

            return build;
        }

        [Benchmark]
        public string StringAdd100Times()
        {
            string build = "";

            for (int i = 0; i < 100; i++)
            {
                build += "dupa";
            }

            return build;
        }

        [Benchmark]
        public string StringAdd1000Times()
        {
            string build = "";

            for (int i = 0; i < 1000; i++)
            {
                build += "dupa";
            }
            return build;
        }

        [Benchmark]
        public string StringBuilderAppend10Times()
        {
            StringBuilder stringBuilder = new StringBuilder();

            for (int i = 0; i < 10; i++)
            {
                stringBuilder.Append("dupa");
            }
            return stringBuilder.ToString();
        }

        [Benchmark]
        public string StringBuilderAppend100Times()
        {
            StringBuilder stringBuilder = new StringBuilder();

            for (int i = 0; i < 100; i++)
            {
                stringBuilder.Append("dupa");
            }
            return stringBuilder.ToString();
        }

        [Benchmark]
        public string StringBuilderAppend1000Times()
        {
            StringBuilder stringBuilder = new StringBuilder();

            for (int i = 0; i < 1000; i++)
            {
                stringBuilder.Append("dupa");
            }
            return stringBuilder.ToString();
        }
    }
}